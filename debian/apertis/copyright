Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright:
 2017       Codecoup
 2023       Cruise LLC.
 2020       Daniel Wagner <dwagner@suse.de>
 2019       Geanix
 2011-2022  Intel Corporation
 2001       Niels Möller
License: LGPL-2.1+

Files: aclocal.m4
Copyright: 1996-2021, Free Software Foundation, Inc.
License: (FSFULLR or GPL-2+) with Autoconf-data exception

Files: build-aux/*
Copyright: 1996-2021, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: build-aux/install-sh
Copyright: 1994, X Consortium
License: X11

Files: build-aux/libtool.m4
Copyright: 1996-2001, 2003-2019, 2021, 2022, Free Software
License: (FSFULLR or GPL-2) with Libtool exception

Files: build-aux/ltmain.sh
Copyright: 1996-2019, 2021, 2022, Free Software Foundation, Inc.
License: (Expat or GPL-2+) with Libtool exception

Files: build-aux/ltoptions.m4
 build-aux/lt~obsolete.m4
Copyright: 2004, 2005, 2007-2009, 2011-2019, 2021, 2022, Free
License: FSFULLR

Files: build-aux/ltsugar.m4
Copyright: 2004, 2005, 2007, 2008, 2011-2019, 2021, 2022, Free Software
License: FSFULLR

Files: build-aux/ltversion.m4
Copyright: 2004, 2011-2019, 2021, 2022, Free Software Foundation
License: FSFULLR

Files: configure
Copyright: 1992-1996, 1998-2017, 2020-2023, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: 2022-2024, Jonas Smedegaard <dr@jones.dk>
 2016-2022, Nobuhiro Iwamatsu <iwamatsu@debian.org>
License: LGPL-2.1+

Files: debian/tests/*
Copyright: 2020, Collabora Ltd.
License: GPL-2

Files: debian/tests/control
Copyright: 2022-2024, Jonas Smedegaard <dr@jones.dk>
 2016-2022, Nobuhiro Iwamatsu <iwamatsu@debian.org>
License: LGPL-2.1+

Files: ell/*
Copyright: 2011-2022, Intel Corporation
License: LGPL-2.1

Files: ell/dbus-client.c
 ell/dbus-client.h
Copyright: 2017, Codecoup
 2011-2014, Intel Corporation
License: LGPL-2.1

Files: ell/ecc-external.c
Copyright: 2013, Kenneth MacKay
License: BSD-2-clause

Files: ell/gpio.c
 ell/gpio.h
Copyright: 2019, Geanix
License: LGPL-2.1

Files: ell/minheap.c
 ell/sysctl.c
 ell/sysctl.h
Copyright: 2023, Cruise LLC
 2012, Intel Corporation
License: LGPL-2.1

Files: ell/minheap.h
Copyright: 2023, Cruise LLC
License: LGPL-2.1

Files: ell/notifylist.c
 ell/notifylist.h
Copyright: 2024, Cruise, LLC
License: LGPL-2.1

Files: ell/utf8.c
 ell/utf8.h
Copyright: 2024, Cruise, LLC
 2011-2014, Intel Corporation
License: LGPL-2.1

Files: examples/*
Copyright: 2011-2022, Intel Corporation
License: LGPL-2.1

Files: linux/*
Copyright: 2016, Linus Walleij
License: GPL-2

Files: tools/*
Copyright: 2011-2022, Intel Corporation
License: LGPL-2.1

Files: tools/gpio.c
Copyright: 2019, Geanix
License: LGPL-2.1

Files: unit/*
Copyright: 2011-2022, Intel Corporation
License: LGPL-2.1

Files: unit/test-minheap.c
 unit/test-sysctl.c
Copyright: 2023, Cruise LLC
License: LGPL-2.1

Files: unit/test-rtnl.c
Copyright: 2020, Daniel Wagner <dwagner@suse.de>
 2011-2014, Intel Corporation
License: LGPL-2.1

Files: unit/test-utf8.c
Copyright: 2024, Cruise, LLC
 2011-2014, Intel Corporation
License: LGPL-2.1

Files: INSTALL Makefile.in
Copyright:
 2017       Codecoup
 2023       Cruise LLC.
 2020       Daniel Wagner <dwagner@suse.de>
 2019       Geanix
 2011-2022  Intel Corporation
 2001       Niels Möller
License: LGPL-2.1+
